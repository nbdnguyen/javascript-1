import {laptops} from "./laptops.js";

//DOM elements
const dmBalance = document.getElementById("balance");
const dmLoanButt = document.getElementById("loanbutt");
const dmPay = document.getElementById("pay");
const dmBankButt = document.getElementById("bankbutt");
const dmWorkButt = document.getElementById("workbutt");
const dmLaptop = document.getElementById("laptop");
const dmFeat = document.getElementById("features");
const dmSelLaptop = document.getElementById("selLaptop");
const dmDescription = document.getElementById("description");
const dmPrice = document.getElementById("price");
const dmBuyButt = document.getElementById("buybutt");
const dmImg = document.getElementById("img");

//Buttons
let loans = true; //If true, customer can get a loan. If false, customer has to buy a laptop.
dmLoanButt.addEventListener("click", function(event) {
    //Opens popup to input amount.
    //Can not loan more than 2x balance.
    //Can not get more than one loan before buying laptop. 

    if (loans) {
        var loanAmount = parseInt(prompt("How much do you want to loan?", "Max: " + 2*curBalance));
        if (isNaN(loanAmount)) {
            //Catch empty input, do nothing.
        } else if (loanAmount > 2*curBalance) {
            alert("Can not loan more than twice the current balance!");
        } else if (loanAmount != 0) {
            console.log(loanAmount);
            loans = false;
            curBalance = updateAmount(curBalance, loanAmount);
        }
    } else {
        alert("Have to buy a laptop before an other loan!");
    }
    render();
});

dmBankButt.addEventListener("click", function(event) {
    //Moves pay to balance and resets pay.
    curBalance = updateAmount(curBalance, curPay);
    curPay = 0;
    render();
});

dmWorkButt.addEventListener("click", function(event) {
    //Increases pay with 100kr per click.
    curPay = updateAmount(curPay, 100);
    render();   
});

dmBuyButt.addEventListener("click", function(event) {
    //Buys laptop
    //If insufficient balance, show error message.
    //If sufficient balance, subtract from balance, show success.
    if (dmLaptop.value == "") {
        alert("Can not buy nothing valued nothing! Pick a laptop first.");
    } else if (curBalance < parseInt(dmPrice.innerText)) {
        alert("Insufficient funds! Go work!");
    } else {
        curBalance = updateAmount(curBalance, -parseInt(dmPrice.innerText));
        alert("Laptop purchased! Enjoy the monstrosity.");
        loans = true;
    }
    render();
});

//Updates values and texts
let curBalance = 0;
let curPay = 0;
dmLaptop.onchange = function() {
    let key = dmLaptop.value;
    let {name, feat, img, desc, price} = laptops[key];
    dmSelLaptop.innerText = name;
    dmFeat.innerText = feat;
    dmImg.innerHTML = img;
    dmDescription.innerText = desc;
    dmPrice.innerText = price;
}

function render() {
    dmPay.innerText = curPay;   
    dmBalance.innerText = curBalance;
}
const updateAmount = (curTotal, addedAmount) => curTotal + addedAmount;