export const laptops = {
     pear: {
          name: "Pear Infusion",
          feat: `Not an apple!
               Can only load social media!
               More expensive!`,
          img: '<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:\
               ANd9GcT-1BpIzHbbYNMM6ATh77ukQfv_nGdDGVyQ2FLcgO2cxm8KS52M&s"\
               alt="A pear laptop." style="width:200px;height:200px;">',
          desc: "Rivals the Apple laptops by having a completely different design.\
               Be different, be hipster!",
          price: 14999
          
     },
     banana: {
          name: "Banana Problemo",
          feat: "Very flexible.\n Can bend up to five times!",
          img: '<img src="https://images-na.ssl-images-amazon.com/images/I/41qtpKtagIL.jpg"\
                alt="A banana laptop." style="width:200px;height:200px;">',
          desc: "Most flexible and bendable laptop on the market.\
                Functionality after bending not guaranteed.",
          price: 1399
     },
     straw: {
          name: "Strawberry Xplosion",
          feat: "Has an intense sweet smell.\n Perfect for first laptop users!\
               \n Nose-cringe inducing.",
          img: '<img src="https://i.ebayimg.com/images/g/BPIAAOSwhqhZ7EiQ/s-l300.jpg"\
               alt="A strawberry." style="width:200px;height:200px;">',
          desc: "Has the strongest smell of any laptop on the market.\
               Turn heads by using this smell bomb!",
          price: 500
     },
     pine: {
          name: "Pineapple Tank",
          feat: "Strong as a tank.\n Waterproof!\n Can drop on hard concrete.",
          img: '<img src="https://thumbs.dreamstime.com/b/laptop-computer-pineapple-3076415.jpg"\
               alt="A pineapple laptop" style="width:200px;height:200px;">',
          desc: "Built for destruction. Want to nuke a building?\
               Chuck the laptop at it. Do it, I dare you.",
          price: 20000
     }   
}