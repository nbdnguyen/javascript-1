# JavaScript task 1

This is the first task for JavaScript class.

A webpage for selling laptops.

Includes:

* Bank profile: **Name**, **balance** and a **loan button** that lets you loan once before purchase, max 2*current balance.

* Work profile: Accumulated **pay** after working. A **banking** and a **work button**. Work adds 100kr, and bank moves pay to balance.

* Laptop select: Select **drop down menu** of laptops and a list of their **features**.

* Selected laptop: **Information** about the current laptop, with **photo** and **price** and a **purchase button**.